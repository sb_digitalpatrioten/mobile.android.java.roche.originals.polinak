package com.example.polina.rochesystem.domain;

import com.google.gson.annotations.SerializedName;

/**
 * POJO object storing configuration used when the app connects to
 * the Roche system for requesting its access token.
 *
 * Created by Polina Koleva on 8/25/16.
 */
public class UserCredentials {

    private static final String OAUTH_CLIENT_ID = "7a56725d7005e42358e5de68f6178812ef03d524eea30b61218585f43085eb73";
    private static final String OAUTH_CLIENT_SECRET_KEY= "73ddb3992b650045a04d66affc304b4e2b6d7d888e77ee410c999ffee05046ee";
    private static final String GRANT_TYPE = "myGrandType";
    private static final String USERNAME = "username";
    private static final String PASSWORD = "8e5026b680912168c7e9316e111669c826";

    @SerializedName("username")
    private String username;

    @SerializedName("password")
    private String password;

    @SerializedName("grant_type")
    private String grantType;

    @SerializedName("client_id")
    private String clientId;

    @SerializedName("client_secret")
    private String clientSecretKey;


    // default constructor initializing the object with already given configurations
    public UserCredentials(){
        this.username = USERNAME;
        this.password = PASSWORD;
        this.grantType = GRANT_TYPE;
        this.clientId = OAUTH_CLIENT_ID;
        this.clientSecretKey = OAUTH_CLIENT_SECRET_KEY;
    }
}
