package com.example.polina.rochesystem.network;

import com.example.polina.rochesystem.domain.AccessToken;
import com.example.polina.rochesystem.domain.Product;
import com.example.polina.rochesystem.domain.UserCredentials;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Class that provides easy way of requesting the Roche system's API. Every request that is needed and
 * accessible by out application is placed in this class as a method declaration.
 *
 * Created by Polina Koleva on 8/25/16.
 */
public interface RestApiEndPointInterface {

    /**
     * Authenticates into the Roche system by acquiring an access token.
     *
     * @param credentials object stores the user credentials needed for accessing the Roche system.
     * @return
     */
    @POST("/api/v1/doccheck/token")
    Call<AccessToken> getAccessToken(@Body UserCredentials credentials);


    /**
     * Gets information for a product by its product id.
     *
     * @param productId identifier of a product for which information will be requested from the server
     * @return product information wrapper into a class {@link Product}
     */
    @GET("/api/v1/products/{product_id}")
    Call<Product> getProduct(@Path("product_id") String productId);


}
