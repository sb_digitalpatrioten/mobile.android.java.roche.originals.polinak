package com.example.polina.rochesystem;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ButtonBarLayout;
import android.view.View;
import android.widget.Button;

import com.example.polina.rochesystem.domain.AccessToken;
import com.example.polina.rochesystem.domain.UserCredentials;
import com.example.polina.rochesystem.network.RestApiEndPointInterface;
import com.example.polina.rochesystem.network.RetrofitManager;
import com.example.polina.rochesystem.utils.Constants;
import com.google.gson.Gson;
import com.orhanobut.logger.Logger;

import org.parceler.Parcels;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    public static String ACCESS_TOKEN = "accessToken";
    private static String ACCESS_TOKEN_OBJECT_KEY = "accessTokenObjectKey";

    // register a broadcast receiver to receive when the request for authentication
    // finishes successfully
    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            // moves to the next activity
            Intent createItemsActivity = new Intent(MainActivity.this, ItemsNavigationDrawerActivity.class);
            createItemsActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            AccessToken token = getAccessToken();
            createItemsActivity.putExtra(ACCESS_TOKEN, Parcels.wrap(token));
            startActivity(createItemsActivity);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onResume(){
        super.onResume();
        registerReceiver(receiver, new IntentFilter(Constants.AUTHENTICATION_REQUEST_FINISHED_BROADCAST));
    }

    protected  void onPause(){
        super.onPause();
        unregisterReceiver(receiver);
    }
    /**
     * Method executed when the button "Authenticate" positioned in the main activity is pressed.
     * The implementation sends a request to Roche System for getting credentials for future access.
     *
     * @param view view on which the clicked button is positioned
     */
    public void authenticate(final View view){

        // hide the button after it is clicked
        view.setVisibility(View.GONE);

        // start the progress bar because we are making async call to the Roche system
        View progressBarView = findViewById(R.id.authentication_progress_view);
        progressBarView.setVisibility(View.VISIBLE);

        Logger.d("Authenticate...");

        UserCredentials userCredentials = new UserCredentials();
        RestApiEndPointInterface service = RetrofitManager.getRestApiService();
        Call<AccessToken> authentication = service.getAccessToken(userCredentials);
        authentication.enqueue(new Callback<AccessToken>() {

            @Override
            public void onResponse(Call<AccessToken> call, Response<AccessToken> response) {
                int statusCode = response.code();
                Logger.d("Authentication completed successfully. Response code:" + statusCode);
                AccessToken accessToken = response.body();
                storeAccessToken(accessToken, ACCESS_TOKEN_OBJECT_KEY);

                // hides the progress bar
                View progressBarView = findViewById(R.id.authentication_progress_view);
                progressBarView.setVisibility(View.GONE);

                sendBroadcast(new Intent(Constants.AUTHENTICATION_REQUEST_FINISHED_BROADCAST));

            }

            @Override
            public void onFailure(Call<AccessToken> call, Throwable t) {
                Logger.e("Problem occurs at authentication: " + t.getCause().toString());

                // hides the progress bar
                View progressBarView = findViewById(R.id.authentication_progress_view);
                progressBarView.setVisibility(View.GONE);

                final Button authenticateButton = (Button) findViewById(R.id.authenticate_button);

                // shows error message on the dismissal of the alert show again the button
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(authenticateButton.getContext());
                alertBuilder.setMessage(Constants.ERROR_MESSAGE);
                alertBuilder.setCancelable(true);
                alertBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        authenticateButton.setVisibility(View.VISIBLE);
                    }
                });
                AlertDialog errorAlert = alertBuilder.create();
                errorAlert.show();
            }

        });
    }

    /**
     * Stores an access token object into a shared preferences.
     *
     * @param token
     * @param accessTokenObjectKey
     */
    private void storeAccessToken(AccessToken token, String accessTokenObjectKey){
        SharedPreferences preferences = getApplicationContext().
                getSharedPreferences("myPreferences",Context.MODE_PRIVATE);
        SharedPreferences.Editor preferenceEditor = preferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(token);
        preferenceEditor.putString(accessTokenObjectKey, json);
        preferenceEditor.commit();
    }

    /**
     * Gets an access token object from shared preferences.
     *
     * @return
     */
    private AccessToken getAccessToken(){
        SharedPreferences preferences = getApplicationContext().
                getSharedPreferences("myPreferences",Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = preferences.getString(ACCESS_TOKEN_OBJECT_KEY, "");
        AccessToken accessToken = gson.fromJson(json, AccessToken.class);
        return accessToken;
    }

}
