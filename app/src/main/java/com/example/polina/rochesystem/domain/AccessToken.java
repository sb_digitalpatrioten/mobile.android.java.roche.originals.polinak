package com.example.polina.rochesystem.domain;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.Date;

/**
 * Pojo object that represents information about user access token which is used for
 * authentication in the Roche system.
 *
 * Created by Polina Koleva on 8/25/16.
 */
@Parcel
public class AccessToken {

    @SerializedName("access_token")
    private String token;

    @SerializedName("token_type")
    private String tokenType;


    @SerializedName("refresh_token")
    private String refreshToken;


    public AccessToken(){

    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return this.token;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public String getTokenType() {
        return this.tokenType;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getRefreshToken() {
        return this.refreshToken;
    }


}
