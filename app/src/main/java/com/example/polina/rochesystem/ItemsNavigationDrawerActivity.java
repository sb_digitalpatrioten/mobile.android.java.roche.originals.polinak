package com.example.polina.rochesystem;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import com.example.polina.rochesystem.code.scanner.CodeScanner;
import com.example.polina.rochesystem.domain.AccessToken;
import com.example.polina.rochesystem.domain.Product;
import com.example.polina.rochesystem.domain.ShortProductInfo;
import com.example.polina.rochesystem.network.RestApiEndPointInterface;
import com.example.polina.rochesystem.network.RetrofitManager;
import com.example.polina.rochesystem.utils.Constants;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.mikepenz.fastadapter.adapters.FastItemAdapter;
import com.orhanobut.logger.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcels;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ItemsNavigationDrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static String PRODUCT_LIST = "com.example.polina.rochesystem.PRODUCT_LIST";

    private static int CODE_START_INDEX = 3;
    private static int CODE_LENGTH = 14;

    private List<ShortProductInfo> shortProductInfos;
    private Map<Integer, Product> products;
    private RecyclerView mRecyclerView;
    private FastItemAdapter fastAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items_navigation_drawer);

        // configures the recycle view
        mRecyclerView = (RecyclerView) findViewById(R.id.items_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter
        //create our FastAdapter which will manage everything
        fastAdapter = new FastItemAdapter();

        //set our adapters to the RecyclerView
        mRecyclerView.setAdapter(fastAdapter);

        shortProductInfos = new ArrayList<ShortProductInfo>();
        products = new HashMap<Integer, Product>();

        //retrieve already stored items
        List<Product> retrievedProductList = getProducts(PRODUCT_LIST);
        if (retrievedProductList != null) {
            for (int i = 0; i < retrievedProductList.size(); i++) {
                Product product = retrievedProductList.get(i);
                products.put(product.getId(), product);
                ShortProductInfo productInfo = new ShortProductInfo(product.getName(), product.getId());
                shortProductInfos.add(productInfo);
            }

            //set the items to your ItemAdapter
            fastAdapter.add(shortProductInfos);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        storeProductList(getProducts(), "productList");
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.items_navigation_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * Called when a button "Scan" is clicked. It contains the logic used for scanning codes.
     *
     * @param view view that initialized this action when it's clicked
     */
    public void scan(View view){
        // scans the code
        CodeScanner scanner = new CodeScanner(this);
        scanner.scan();
    }

    // Get the results from scanning the code
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null) {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            } else {

                String parsedCode = result.getContents();
                if (parsedCode.length() <= CODE_START_INDEX + CODE_LENGTH){
                    // show alert that something goes wrong
                    Button scanButton = (Button) findViewById(R.id.scan_button);
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(scanButton.getContext());
                    alertBuilder.setMessage(Constants.ERROR_MESSAGE);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
                    AlertDialog errorAlert = alertBuilder.create();
                    errorAlert.show();
                } else {
                    // initializes the product request
                    getProduct(parsedCode.substring(CODE_START_INDEX, CODE_START_INDEX + CODE_LENGTH));
                }

            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    /**
     * Gets access token which is transferred by the previous activity.
     *
     * @return {@link AccessToken} global object
     */
    private AccessToken getAccessToken(){
        return (AccessToken) Parcels.unwrap(getIntent().getParcelableExtra(MainActivity.ACCESS_TOKEN));
    }

    private void getProduct(String code) {

        RestApiEndPointInterface service = RetrofitManager.getRestApiService();
        Call<Product> productRequest = service.getProduct(code);
        productRequest.enqueue(new Callback<Product>() {

            @Override
            public void onResponse(Call<Product> call, Response<Product> response) {
                int statusCode = response.code();
                Logger.d("Requesting product completed successfully. Response code:" + statusCode);
                Product product = response.body();
                ShortProductInfo shortProductInfo = new ShortProductInfo(product.getName().trim(), product.getId());
                // stores a new product
                shortProductInfos.add(shortProductInfo);
                products.put(product.getId(), product);

                //notifies the adapter
                fastAdapter.add(shortProductInfo);
            }

            @Override
            public void onFailure(Call<Product> call, Throwable t) {
                Logger.e("Problem occurs at authentication: " + t.getCause().toString());
            }
        });
    }

    /**
     * Moves to the activity showing item information.
     */
    private void startItemActivity() {
        Intent itemActivity = new Intent(this, ItemActivity.class);
        startActivity(itemActivity);
    }

    /**
     * Stores a list of products into shared preferences.
     *
     * @param products
     * @param productListKey
     */
    private void storeProductList(List<Product> products, String productListKey){
        SharedPreferences preferences = getApplicationContext().
                getSharedPreferences("myPreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor preferenceEditor = preferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(products);
        JSONArray jsonArrayProduct= new JSONArray();
        try {
            jsonArrayProduct.put(new JSONObject(json));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        preferenceEditor.putString(productListKey, json);
        preferenceEditor.commit();
    }

    /**
     * Retrieves a list of products from shared preferences.
     *
     * @param productListKey
     * @return
     */
    private List<Product> getProducts(String productListKey){
        SharedPreferences preferences = getApplicationContext().
                getSharedPreferences("myPreferences",Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = preferences.getString(productListKey, "");
        Type type = new TypeToken<List<Product>>() {}.getType();
        List<Product> products = gson.fromJson(json, type);
        return products;
    }

    private List<Product> getProducts(){
        ArrayList<Product> productsList = new ArrayList<Product>();
        for ( Map.Entry<Integer, Product> entry : products.entrySet()) {
            Product pr = entry.getValue();
            productsList.add(pr);
        }
        return productsList;
    }
}
