package com.example.polina.rochesystem.domain;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.net.URL;

/**
 * POJO that stores information about advertisement of a product.
 *
 * Created by Polina Koleva on 8/26/16.
 */
@Parcel
public class Advertisement {

    private int id;
    private String title;
    private URL url;
    private AddImage image;

    // default constructor
    public  Advertisement(){

    }
}
