package com.example.polina.rochesystem.utils;

/**
 * Utils class storing all global constants.
 *
 * Created by Polina Koleva on 8/25/16.
 */
public class Constants {

    // broadcast messages
    public static String AUTHENTICATION_REQUEST_FINISHED_BROADCAST = "com.example.polina.rochesystem.AUTHENTICATION.FINISHED";


    //alert messages
    public static String ERROR_MESSAGE = "Oops, a problem occurs. Please retry your action.";
}
