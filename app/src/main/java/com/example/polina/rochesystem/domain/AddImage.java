package com.example.polina.rochesystem.domain;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.net.URL;

/**
 * POJO object that stores information for image of an advertisement.
 *
 * Created by Polina Koleva on 8/26/16.
 */
@Parcel
public class AddImage {

    @SerializedName("url")
    private URL url;

    @SerializedName("width")
    private double width;

    @SerializedName("height")
    private double height;

    public AddImage(){

    }
}
