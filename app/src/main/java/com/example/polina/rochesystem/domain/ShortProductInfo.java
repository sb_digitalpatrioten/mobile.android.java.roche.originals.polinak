package com.example.polina.rochesystem.domain;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.polina.rochesystem.R;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

/**
 * Object that is rendered in the list with products.
 *
 * Created by Polina Koleva on 8/26/16.
 */
public class ShortProductInfo extends AbstractItem<ShortProductInfo, ShortProductInfo.ViewHolder> {

    private String productName;
    private int id;

    public  ShortProductInfo(String productName, int id){
        this.productName = productName;
        this.id = id;
    }

    public String getProductName(){
        return  this.productName;
    }

    public void setProductName(String productName){
        this.productName = productName;
    }

    public int getId(){
        return this.id;
    }

    public void setId(int id){
        this.id = id;
    }

    //The unique ID for this type of item
    @Override
    public int getType() {
        return R.id.unique_item_view;
    }

    //The layout to be used for this type of item
    @Override
    public int getLayoutRes() {
        return R.layout.short_product_information;
    }

    //The logic to bind your data to the view
    @Override
    public void bindView(ViewHolder viewHolder, List payloads) {
        //call super so the selection is already handled for you
        super.bindView(viewHolder, payloads);

        //bind our data
        //set the text for the name
        viewHolder.name.setText(productName);
    }

    // The viewHolder used for this item.
    // This viewHolder is always reused by the RecyclerView so scrolling is blazing fast
    protected static class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView name;

        public ViewHolder(View view) {
            super(view);
            this.name = (TextView) view.findViewById(R.id.product_name);
        }
    }
}
