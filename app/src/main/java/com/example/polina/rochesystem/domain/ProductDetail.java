package com.example.polina.rochesystem.domain;

import org.parceler.Parcel;

/**
 * POJO object that stores information for different details about a product.
 *
 * Created by Polina Koleva on 8/26/16.
 */
@Parcel
public class ProductDetail {

    private String title;
    private String value;
    private String iconUrl;

    // default constructor
    public  ProductDetail(){

    }
}
