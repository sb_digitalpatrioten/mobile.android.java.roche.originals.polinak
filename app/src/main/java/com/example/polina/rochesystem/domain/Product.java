package com.example.polina.rochesystem.domain;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.polina.rochesystem.R;
import com.google.gson.annotations.SerializedName;
import com.mikepenz.fastadapter.items.AbstractItem;

import org.parceler.Parcel;


import java.net.URL;
import java.util.List;

/**
 * POJO object represents stored information for each product.
 * <p/>
 * Created by Polina Koleva on 8/25/16.
 */
@Parcel
public class Product {

    @SerializedName("id")
    private  int id;

    @SerializedName("name")
    private String name;

    @SerializedName("image")
    private URL image;

    @SerializedName("pid")
    private String pid;

    @SerializedName("application_notes_url")
    private URL applicationNotesURL;

    @SerializedName("details")
    private ProductDetail[] details;

    @SerializedName("error")
    private String error;

    @SerializedName("advertisement")
    private Advertisement advertisement;

    // default empty constructor
    public Product() {

    }

    public int getId(){
        return this.id;
    }

    public void setId(int id){
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setImage(URL image) {
        this.image = image;
    }

    public URL getImage() {
        return this.image;
    }


    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getPid() {
        return this.pid;
    }

    public void setApplicationNotesURL(URL applicationNotesURL) {
        this.applicationNotesURL = applicationNotesURL;
    }

    public URL getApplicationNotesURL() {
        return this.applicationNotesURL;
    }

    public void setDetails(ProductDetail[] details) {
        this.details = details;
    }

    public ProductDetail[] getDetails() {
        return this.details;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getError() {
        return this.error;
    }

    public void setAdvertisement(Advertisement advertisement) {
        this.advertisement = advertisement;
    }

    public Advertisement getAdvertisement() {
        return this.advertisement;
    }
}
