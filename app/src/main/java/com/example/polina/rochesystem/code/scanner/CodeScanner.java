package com.example.polina.rochesystem.code.scanner;

import android.app.Activity;

import com.google.zxing.integration.android.IntentIntegrator;

/**
 * Class used as a mediator for scanning codes.
 *
 * Created by Polina Koleva on 8/26/16.
 */
public class CodeScanner {

    private IntentIntegrator intentIntegrator;

    // default code scanner constructor
    public CodeScanner(Activity activity){
        this.intentIntegrator = new IntentIntegrator(activity);
        this.intentIntegrator.setOrientationLocked(false);
        this.intentIntegrator.setBeepEnabled(false);
    }

    /**
     * Scans a code.
     */
    public void scan(){
        this.intentIntegrator.initiateScan();
    }
}
