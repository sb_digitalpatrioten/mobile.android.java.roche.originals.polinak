package com.example.polina.rochesystem.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Class that holds two singleton instances of a Retrofit object and its API service.
 * The latter cannot exist without the former.
 *
 * Created by Polina Koleva on 8/25/16.
 */
public class RetrofitManager {

    // Base url address of the Roche System
    private static final String BASE_URL = "http://staging-roche-serialization.herokuapp.com";

    // singleton instance of a Retrofit object
    private static Retrofit retrofitInstance = null;
    // singleton instance of API service
    private static RestApiEndPointInterface apiService = null;

    /**
     * Gets a Retrofit instance.
     *
     * @return a retrofit instance.
     */
    public static Retrofit getRetrofit() {
        if (retrofitInstance == null) {
            retrofitInstance = new Retrofit.Builder().
                    baseUrl(BASE_URL).
                    addConverterFactory(GsonConverterFactory.create()).
                    build();
            apiService = retrofitInstance.create(RestApiEndPointInterface.class);
        }
        return retrofitInstance;
    }

    /**
     * Gets a api service instance.
     *
     * @return api service instance
     */
    public static RestApiEndPointInterface getRestApiService() {
        if (apiService == null) {
            getRetrofit();
        }
        return apiService;
    }

}
